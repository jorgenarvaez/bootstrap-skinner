'use strict';

// Include Gulp
var gulp = require('gulp');

// Base Folders
var src = 'src/';  // Source Files
var dist = 'dist/'; // Distribution Folder

// Plugins
var rename = require('gulp-rename');
var concat = require('gulp-concat');
var connect = require('gulp-connect');
var watch = require('gulp-watch');

// LESS Plugins
var less = require('gulp-less');
var path = require('path');

// Style Plugins
var autoprefixer = require('gulp-autoprefixer');
var csslint = require('gulp-csslint');
var minifyCSS = require('gulp-minify-css');

// JS Plugins
var jshint = require('gulp-jshint');
//var stylish = require('jshint-stylish');
var uglify = require('gulp-uglify');

// Image Optimization Plugins
var imagemin = require('gulp-imagemin');
var pngquant = require('imagemin-pngquant');

// HTML Minify Plugin
var minifyHTML = require('gulp-minify-html');

///////////
// Tasks //
///////////

// Connect
gulp.task('connect', function() {
  connect.server({
    livereload: true,
    root: [dist],
    host: 'localhost'
  });
});

// Bootstrap LESS Base files Preprocessing
gulp.task('bootstrap-less', function () {
  gulp.src(src + 'less/bootstrap/bootstrap.less')
    .pipe(less({
      paths: [src + 'less']
    }))
    .pipe(autoprefixer('last 5 versions', 'ie 8'))
    .pipe(minifyCSS({keepBreaks: false}))
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest(dist +'css'));
});

// LESS Preprocessing
gulp.task('less', function () {
  gulp.src(src + 'less/main.less')
    .pipe(less({
      paths: [src + 'less']
    }))
    .pipe(autoprefixer('last 5 versions', 'ie 8'))
    .pipe(minifyCSS({keepBreaks: false}))
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest(dist +'css'))
    .pipe(connect.reload());
});

// Bootstrap JS Concatenate & Minify
gulp.task('bootstrap-scripts', function() {
    return gulp.src(src + 'js/bootstrap/*.js')
      .pipe(concat('bootstrap.js'))
        .pipe(rename({suffix: '.min'}))
        .pipe(uglify())
        .pipe(gulp.dest(dist + 'js'));
});

// Vendor (jQuery) JS Minify
gulp.task('jquery-scripts', function() {
    return gulp.src(src + 'js/vendor/jquery-*.js')
      .pipe(rename({basename: 'jquery'}))
      .pipe(rename({suffix: '.min'}))
      .pipe(uglify())
      .pipe(gulp.dest(dist + 'js'));
});

// Vendor (Modernizr) JS Minify
gulp.task('modernizr-scripts', function() {
    return gulp.src(src + 'js/vendor/modernizr-*.js')
      .pipe(rename({basename: 'modernizr'}))
      .pipe(rename({suffix: '.min'}))
      .pipe(uglify())
      .pipe(gulp.dest(dist + 'js'));
});

// JS Concatenate & Minify
gulp.task('scripts', function() {
    return gulp.src(src + 'js/*.js')
      .pipe(concat('main.js'))
        .pipe(rename({suffix: '.min'}))
        .pipe(uglify())
        .pipe(gulp.dest(dist + 'js'))
        .pipe(connect.reload());
});

// Image Optimization
gulp.task('images', function() {
  return gulp.src(src + 'img/**/*')
  .pipe(imagemin({
      progressive: true,
      optimizationLevel: 5,
      svgoPlugins: [{removeViewBox: false}],
      use: [pngquant()]
  }))
  .pipe(gulp.dest(dist + 'img/'));
});

// HTML Minify
gulp.task('minify-html', function() {
  var opts = {
    conditionals: true,
    spare:true
  };
 
  return gulp.src(src + '*.html')
    .pipe(minifyHTML(opts))
    .pipe(gulp.dest( dist ))
    .pipe(connect.reload());
});

// Watch
gulp.task('watch', function() {
  gulp.watch(src + 'js/**/*.js', ['scripts']);
  gulp.watch(src + 'less/**/*.less', ['less']);
  gulp.watch(src + 'images/**/*', ['images']);
  gulp.watch(src + '*.html', ['minify-html']);
});

// CSS Lint
gulp.task('csslint', function() {
  gulp.src(src + 'css/*.css')
    .pipe(csslint())
    .pipe(csslint.reporter())
    .pipe(csslint.failReporter());
});

// LiveReload Task
gulp.task('livereload', function() {
  gulp.src([dist + 'css/*.css', dist + 'js/*.js', dist + '*.html'])
    .pipe(watch([dist + 'css/*.css', dist + 'js/*.js', dist + '*.html']))
    .pipe(connect.reload());
});


// Base Build (Bootstrap + Vendor)
gulp.task('base-build', ['bootstrap-less', 'bootstrap-scripts', 'jquery-scripts', 'modernizr-scripts']);

// Default Task
gulp.task('default', ['less', 'scripts', 'images', 'minify-html', 'connect', 'livereload', 'watch']);